FROM nvidia/cuda:9.0-devel-ubuntu16.04

### Dockerfile for tf-benchamrks, GPU version + Horovod + OpenMPI 4.0
### Based on https://github.com/horovod/horovod/blob/master/Dockerfile
LABEL maintainer="Adrian Grupp, Valentin Kozlov"\
      description="Tensorflow CNN Benchmark for GPU usage + Horovod"

# TensorFlow version is tightly coupled to CUDA and cuDNN so it should be selected carefully
ENV TF_VERSION=1.12
ENV CUDNN_VERSION=7.4.1.5-1+cuda9.0
ENV NCCL_VERSION=2.3.7-1+cuda9.0

# Python 2.7 or 3.5 is supported by Ubuntu Xenial out of the box
ARG python=3.5
ENV PYTHON_VERSION=${python}

RUN apt-get update && apt-get install -y --allow-downgrades --allow-change-held-packages --no-install-recommends \
        build-essential \
        cmake \
        git \
        curl \
        nano \
        wget \
        ca-certificates \
        libcudnn7=${CUDNN_VERSION} \
        libnccl2=${NCCL_VERSION} \
        libnccl-dev=${NCCL_VERSION} \
        libjpeg-dev \
        libpng-dev \
        python${PYTHON_VERSION} \
        python${PYTHON_VERSION}-dev \
        openssh-client && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /root/.cache/pip/* && \
    rm -rf /tmp/* 

RUN ln -s /usr/bin/python${PYTHON_VERSION} /usr/bin/python

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py

# Install TensorFlow, Keras + a few packages for TF Benchmarks
RUN pip --no-cache-dir install 'numpy<1.15.0' \
    tensorflow-gpu==${TF_VERSION}.0 keras h5py \
    future requests \
    py-cpuinfo psutil && \
    rm -rf /root/.cache/pip/* && \
    rm -rf /tmp/*

# Install Open MPI
RUN mkdir /tmp/openmpi && \
    cd /tmp/openmpi && \
    wget https://www.open-mpi.org/software/ompi/v4.0/downloads/openmpi-4.0.0.tar.gz && \
    tar zxf openmpi-4.0.0.tar.gz && \
    cd openmpi-4.0.0 && \
    ./configure --enable-orterun-prefix-by-default && \
    make -j $(nproc) all && \
    make install && \
    ldconfig && \
    rm -rf /tmp/openmpi

# Install Horovod, temporarily using CUDA stubs
RUN ldconfig /usr/local/cuda-9.0/targets/x86_64-linux/lib/stubs && \
    HOROVOD_GPU_ALLREDUCE=NCCL HOROVOD_WITH_TENSORFLOW=1 pip install --no-cache-dir horovod && \
    ldconfig

### TF Benchmarks config ###
ENV DATA_DIR  /benchmarks/data
ENV TRAIN_DIR /benchmarks/output
ENV LOG_DIR   /benchmarks/logs

### Create and set the working directory to /benchmarks
WORKDIR /benchmarks

### Clone tf_cnn_benchmarks from the official repository into /benchamrks
RUN git clone -b cnn_tf_v${TF_VERSION}_compatible https://github.com/tensorflow/benchmarks.git /benchmarks.tmp && \
    mv -T /benchmarks.tmp/scripts/tf_cnn_benchmarks /benchmarks && rm -rf /benchmarks.tmp

# Copy one directory from tensorflow/models
# ATTENTION! tensorflow/models is huge, ca. 1.1GB, 
# trying to copy in "light way" but still ca.500MB
RUN mkdir /models.tmp && cd /models.tmp && git init && \
    git remote add origin https://github.com/tensorflow/models.git && \
    git fetch origin && \
    git checkout origin/r${TF_VERSION}.0 models/official/utils/logs && \
    mv official /benchmarks && \
    rm -rf /models.tmp

### Run tf_cnn_benchmarks.py with default parameters when the container launches
### Uses synthetic data without --data_dir specified
CMD ["python", "tf_cnn_benchmarks.py", "--batch_size=64", "--model=resnet50", "--num_gpus=1", "--variable_update=parameter_server"]
