FROM tensorflow/tensorflow:1.10.0-py3

### Dockerfile for tf-benchamrks, CPU version
LABEL maintainer="Adrian Grupp, Valentin Kozlovdevice"\
      description="Tensorflow CNN Benchmark for CPU usage"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt install -y --no-install-recommends git && \ 
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /root/.cache/pip/* && \
    rm -rf /tmp/* 
# git optional, if copied directly

RUN pip --no-cache-dir install future requests \
    py-cpuinfo psutil && \
    rm -rf /root/.cache/pip/* && \
    rm -rf /tmp/*

### For CUDA profiling, TensorFlow requires CUPTI.
#ENV LD_LIBRARY_PATH /usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH
ENV DATA_DIR  /benchmarks/data
ENV TRAIN_DIR /benchmarks/output
ENV LOG_DIR   /benchmarks/logs

### Create and set the working directory to /benchmarks
WORKDIR /benchmarks

### Copy the current directory contents into the container at /benchmarks
## => instead clone it from github. see below
COPY . /benchmarks
### Install latest version of tf-benchmarks:
# RUN git clone https://github.com/vykozlov/tf-benchmarks /benchmarks

### Run tf_cnn_benchmarks.py with default parameters when the container launches
### Uses synthetic data without --data_dir specified
CMD ["python3", "tf_cnn_benchmarks.py", "--batch_size=64", "--model=resnet50", "--device=cpu", "--data_format=NHWC", "--variable_update=parameter_server"]
