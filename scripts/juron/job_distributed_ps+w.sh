#!/bin/bash

#TIMESTAMP=$(date +%s)
TIMESTAMP=$(date +%y%m%d_%H%M%S)
ThisHOST=$(hostname -i)

echo $ThisHOST:$PARAM_SERVER_PORT >> $TFBENCH_OUTPUT/param_servers.list
echo $ThisHOST:$WORKER_PORT >> $TFBENCH_OUTPUT/workers.list

# parameter servers
TRAIN_DIR=$TFBENCH_RUN/ps${ParamServerID}-output
LOG_DIR=$TFBENCH_RUN/ps${ParamServerID}-log

# Check if all param server jobs has started

n_param_servers_on=0
while [ $n_param_servers_on -lt $NumParamServers ]
do
    sleep 5
    PARAM_SERVERS=($(cat $TFBENCH_OUTPUT/param_servers.list))
    n_param_servers_on=${#PARAM_SERVERS[@]}
done

# Check if all worker jobs has started
n_workers_on=0
while [ $n_workers_on -lt $NumWorkers ]
do
    sleep 5
    WORKERS=($(cat $TFBENCH_OUTPUT/workers.list))
    n_workers_on=${#WORKERS[@]}
done

# https://stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash
PARAM_SERVERS_LIST=$(IFS=, ; echo "${PARAM_SERVERS[*]}")
#echo $PARAM_SERVERS_LIST

WORKERS_LIST=$(IFS=, ; echo "${WORKERS[*]}")
#echo $WORKERS_LIST

OPTIONS="--batch_size=$BATCH_SIZE \
--optimizer=sgd \
--variable_update=distributed_replicated \
--cross_replica_sync=True \
--local_parameter_device=gpu \
--num_gpus=1 \
--model=$MODEL \
$DATASET_OPTIONS \
--ps_hosts=$PARAM_SERVERS_LIST \
--worker_hosts=$WORKERS_LIST"

OPTIONS_PS="${OPTIONS} \
--train_dir=$TRAIN_DIR \
--log_dir=$LOG_DIR \
--benchmark_log_dir=$LOG_DIR \
--benchmark_test_id=ps_${TIMESTAMP} \
--job_name=ps \
--task_index=$ParamServerID"

### NOW THE WORKER PART ###
TRAIN_DIR=$TFBENCH_RUN/worker${WorkerID}-output
LOG_DIR=$TFBENCH_RUN/worker${WorkerID}-log

OPTIONS_WORKER="${OPTIONS} \
--train_dir=$TRAIN_DIR \
--log_dir=$LOG_DIR \
--benchmark_log_dir=$LOG_DIR \
--benchmark_test_id=worker_${TIMESTAMP} \
--job_name=worker \
--task_index=$WorkerID"

export CUDA_VISIBLE_DEVICES=0,1
echo "[INFO] param_serverID=$ParamServerID from $NumParamServers"
echo "python tf_cnn_benchmarks.py $OPTIONS_PS"
python tf_cnn_benchmarks.py $OPTIONS_PS &

sleep 60
export CUDA_VISIBLE_DEVICES=2,3
echo "[INFO] workerID=$WorkerID from $NumWorkers"
echo "python tf_cnn_benchmarks.py $OPTIONS_WORKER"
python tf_cnn_benchmarks.py $OPTIONS_WORKER
