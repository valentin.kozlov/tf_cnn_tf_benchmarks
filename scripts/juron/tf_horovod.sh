#!/bin/bash

TIMESTAMP=$(date +%y%m%d_%H%M%S)

HOST_RUN_RESULTS=$HOME/benchmarks.1

### Model Training Settings
DATA_DIR=$HOME/datasets
DATASET=synthetic        # imagenet
NUM_BATCHES=50
NUM_EPOCHS=1
export MODEL=resnet50
export BATCH_SIZE=64  #64

### JOB parameters
NUM_HOSTS=1        # number of hosts to use
NUM_GPUS=4         # number of GPUs per host

export NUM_PROCESS=$(expr $NUM_HOSTS '*' $NUM_GPUS)

export HOST_RUN=$HOST_RUN_RESULTS/${TIMESTAMP}_horovod_${DATASET}_${MODEL}_${NUM_HOSTS}hosts
# check if local directory exist
if [ ! -d "$HOST_RUN" ]; then
   mkdir -p $HOST_RUN
fi

if [ "$DATASET" != "synthetic" ]; then
    export DATASET_OPTIONS="--data_name=$DATASET --data_dir=$DATA_DIR --num_epochs=$NUM_EPOCHS"
else
    export DATASET_OPTIONS="--num_batches=$NUM_BATCHES"
fi

module load python/3.6.1 tensorflow/1.12.0-gcc_5.4.0-cuda_10.0.130
module load openmpi/3.1.2-gcc_5.4.0-cuda_10.0.130
module load horovod/0.15.2
module load virtualenv
source $HOME/.venv/tf1120/bin/activate

# -n ${NUM_HOSTS}
echo "bsub -R "${NUM_HOSTS}*{span[ptile=1] rusage[ngpus_physical=${NUM_GPUS}]}" -e $HOST_RUN/job_horovod.err -o $HOST_RUN/job_horovod.out ./job_horovod.sh"
bsub -R "${NUM_HOSTS}*{span[ptile=1] rusage[ngpus_physical=${NUM_GPUS}]}" -e $HOST_RUN/job_horovod.err -o $HOST_RUN/job_horovod.out ./job_horovod.sh  
