#!/bin/bash

TIMESTAMP=$(date +%y%m%d_%H%M%S)

rm $HOME/benchmarks.1/workers.list
rm $HOME/benchmarks.1/param_servers.list

export NumParamServers=2
export NumWorkers=2
export NumGPUs=4
export PARAM_SERVER_PORT=2222
export WORKER_PORT=2223

export TFBENCH_OUTPUT=$HOME/benchmarks.1
export TFBENCH_RUN=$TFBENCH_OUTPUT/${TIMESTAMP}-Run
export MODEL=resnet50
export BATCH_SIZE=128  #64

# check if local directories exist
if [ ! -d "$TFBENCH_RUN" ]; then
   mkdir -p $TFBENCH_RUN
fi

### DATA
DATA_DIR=$TFBENCH_OUTPUT/data
DATASET=synthetic        # imagenet
NUM_BATCHES=50
NUM_EPOCHS=1

if [ "$DATASET" != "synthetic" ]; then
    export DATASET_OPTIONS="--data_name=$DATASET --data_dir=$DATA_DIR --num_epochs=$NUM_EPOCHS"
else
    export DATASET_OPTIONS="--num_batches=$NUM_BATCHES"
fi

JOB_INFO_DIR=$HOME/jobs/${TIMESTAMP}-Run
# check if local directories exist
if [ ! -d "$JOB_INFO_DIR" ]; then
   mkdir -p $JOB_INFO_DIR
fi

for (( i=0; i<${NumParamServers}; i++ ));
do
    export TFServerID=$i
    export TF_SERVER_TYPE=ps
    export ParamServerID=$i
    export WorkerID=$i
    bsub -gpu "num=${NumGPUs}:j_exclusive=yes" -e $JOB_INFO_DIR/job_distributed.err -o $JOB_INFO_DIR/job_distributed.out ./job_distributed_ps+w.sh 
    #bsub -gpu "num=${NumGPUs}:j_exclusive=yes" -e $JOB_INFO_DIR/job_param_server.err -o $JOB_INFO_DIR/job_param_server.out ./job_distributed.sh 
    #sleep 10
done

#for (( j=0; j<${NumWorkers}; j++ ));
#do
#    export TFServerID=$j
#    export TF_SERVER_TYPE=worker
#    bsub -gpu "num=${NumGPUs}:j_exclusive=yes" -e $JOB_INFO_DIR/job_workers.err -o $JOB_INFO_DIR/job_workers.out ./job_distributed.sh 
#    sleep 2
#done
wait
