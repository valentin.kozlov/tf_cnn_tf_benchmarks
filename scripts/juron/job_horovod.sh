#!/bin/bash

#TIMESTAMP=$(date +%s)
TIMESTAMP=$(date +%y%m%d_%H%M%S)
ThisHOST=$(hostname -s)

OUTPUT_DIR=$HOST_RUN/horovod-output
LOG_DIR=$HOST_RUN/horovod-log

OPTIONS="--batch_size=$BATCH_SIZE \
--optimizer=sgd \
--variable_update=horovod \
--train_dir=$OUTPUT_DIR \
--log_dir=$LOG_DIR \
--benchmark_log_dir=$LOG_DIR \
--num_gpus=1 \
--model=$MODEL \
$DATASET_OPTIONS \
--benchmark_test_id=horovod_${TIMESTAMP}"

echo "python tf_cnn_benchmarks.py $OPTIONS"

##--oversubscribe
#-bind-to none -map-by slot \
#-x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH -x HOROVOD_MPI_THREADS_DISABLE=1 \
#-mca pml ob1 -mca btl ^openib \

export -n CUDA_VISIBLE_DEVICES

# https://towardsdatascience.com/distributed-tensorflow-using-horovod-6d572f8790c4
MPI_PARAMS="-np $NUM_PROCESS --oversubscribe -bind-to none -map-by slot \
-x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH -x HOROVOD_MPI_THREADS_DISABLE=1 \
-mca pml ob1 -mca btl ^openib"

echo "mpirun  $MPI_PARAMS python tf_cnn_benchmarks.py $OPTIONS"
mpirun $MPI_PARAMS python tf_cnn_benchmarks.py $OPTIONS
