##########
# Script that runs the tf_cnn_benchmark via a udocker container.
# Passed arguments are parameters for the HPC-job.
# Runs one benchmark run as well as one evaluation run and logs the output.
##########

DATENOW=$(date +%y%m%d_%H%M%S)
TESTSMAIN="/srv/tf_cnn_benchmarks-tests"
CIFAR10_DIR="/srv/datasets/cifar10/data"
IMAGENET_DIR="/srv/datasets/imagenet/data"
DATASET_DIR=$IMAGENET_DIR

##### DEFAULT CONFIG #####
NUMGPUS="1"		# Want (1,2,4)
DATASET="synthetic"	# Dataset to use for training (synthetic, cifar10)
MODEL="resnet50"	# Used CNN model (resnet50(for synthetic data), resnet56, alexnet)
NUMEPOCHS=10		# Number of epochs for training with REAL data
NUMBATCHES=500      # Number of epochs for synthetic data
BATCHSIZE=32		# Batch size per GPU, e.g. ResNet, InceptionV3 = 64, AlexNet = 512
# ------------------------
NVIDIA_ENV="CUDA_VISIBLE_DEVICES=0,1,2,3"
# ------------------------

##### USAGEMESSAGE #####
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n
		--num_gpus={1|2|4}	    \t \t \t The amount of GPUs to run on \n
		--dataset={synthetic|cifar10}    \t The Dataset to use \n 
		--data_dir \t Directory for data on host \n 
		--model={resnet50|resnet56|alexnet} \t The CNN model to use \n
		--num_epochs \t number of epochs for training with REAL data (either this or --num_batches, NOT both!) \n 
		--num_batches \t number of batches for training with SYNTHETIC data" 
INFOMESSAGE="=> Should now process scripts"    # ???


##### PARSE SCRIPT FLAGS #####
arr=("$@")
if [ $# -eq 0 ]; then 
# use default config (0)
    break 
elif [ $1 == "-h" ] || [ $1 == "--help" ]; then 
# print usagemessage
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -ge 1 ] && [ $# -le 6 ]; then 
# read benchmark options as parameters (1-3)
    for i in "${arr[@]}"; do
	echo "$i"
        [[ $i = *"--num_gpus"* ]]  && NUMGPUS=${i#*=} 
        [[ $i = *"--dataset"* ]]  && DATASET=${i#*=} 
        [[ $i = *"--data_dir"* ]]  && DATASET_DIR=${i#*=} 
        [[ $i = *"--model"* ]]  && MODEL=${i#*=}
        [[ $i = *"--num_epochs"* ]]  && NUMEPOCHS=${i#*=}
        [[ $i = *"--num_batches"* ]]  && NUMBATCHES=${i#*=}
        [[ $i = *"--batch_size"* ]]  && BATCHSIZE=${i#*=}
    done
else
    # Too many arguments were given (>3)
    echo "ERROR! Too many arguments provided!"
    shopt -s xpg_echo    
    echo $USAGEMESSAGE
    exit 2
fi


##### Create local directories to mount #####

DIR_EXT="${DATASET}_${MODEL}_${NUMGPUS}_gpu" # Directory format for output files

HOST_DATA="$DATASET_DIR"
RUN_DIR="${TESTSMAIN}/${DATENOW}_bmetal_${DIR_EXT}"
HOST_LOG="${RUN_DIR}/log"
HOST_OUTPUT="${RUN_DIR}/output"

# check if local directories exist
if [ ! -d "$RUN_DIR" ]; then
   mkdir $RUN_DIR
   mkdir $HOST_LOG
   mkdir $HOST_OUTPUT
fi
# ------------------------

### !!! comment this out
#if [ "$DATASET" != "synthetic" ]; then
#    HOST_DATA="${HOST_DATA}/${DATASET}"
#fi
###

##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
#--num_epochs=0.25 \
OPTIONS="--batch_size=${BATCHSIZE} \
--optimizer=sgd \
--variable_update=parameter_server \
--local_parameter_device=cpu \
--train_dir=${HOST_OUTPUT} \
--log_dir=${HOST_LOG} \
--benchmark_log_dir=${HOST_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--num_gpus=$NUMGPUS \
--model=$MODEL"		# Has to be resnet50 if synthetic is selected
if [ "$DATASET" != "synthetic" ]; then
# If Data is not synthetic, use data at specified file location
    OPTIONS="${OPTIONS} --data_dir=$HOST_DATA"
    OPTIONS="${OPTIONS} --data_name=$DATASET"
# If Data is not synthetic, use number of epochs
    OPTIONS="${OPTIONS} --num_epochs=${NUMEPOCHS}"
#    OPTIONS="${OPTIONS} --num_batches=${NUMBATCHES}"
fi

if [ "$DATASET" == "synthetic" ]; then
# If Data is synthetic, use number of batches
    OPTIONS="${OPTIONS} --num_batches=${NUMBATCHES}"
fi


EVAL_OPTIONS=" --eval \
--train_dir=${HOST_OUTPUT} \
--benchmark_log_dir=${HOST_LOG} \
--num_gpus=$NUMGPUS \
--model=$MODEL"
#--eval_dir=$PWD/eval/ \ only for TFevent file
if [ "$DATASET" != "synthetic" ]; then
# If Data is not synthetic, use data at specified file location
    EVAL_OPTIONS="${EVAL_OPTIONS} --data_dir=$HOST_DATA"
    EVAL_OPTIONS="${EVAL_OPTIONS} --data_name=$DATASET"
fi


##### RUN THE JOB #####
echo "==================================="
echo "=> Running on: $HOSTNAME"
echo "==================================="

echo "Running benchmark..."

SCRIPT="python3 /srv/tf_cnn_tf_benchmarks/tf_cnn_benchmarks.py $OPTIONS"  # benchmark script to run
$SCRIPT
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/benchmark.log # Rename run_benchmark.log since it is overwritten in eval run 

echo "Done with benchmark. Running evaluation..."

SCRIPT="python3 /srv/tf_cnn_tf_benchmarks/tf_cnn_benchmarks.py $EVAL_OPTIONS"  # evaluation script to run
$SCRIPT
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/eval.log 


### Cleanup
#rm ${HOSTOUTPUT}/*

echo "Done. Logfiles written to: ${HOST_LOG}."
