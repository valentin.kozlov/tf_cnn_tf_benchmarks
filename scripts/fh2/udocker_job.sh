#!/bin/sh
#SBATCH --time=3:00:00
#SBATCH --partition=visu
#SBATCH --nodes=1
#SBTACH --ntasks-per-node=48
#SBATCH --output=${PROJECT}/job.log
#SBATCH --job-name=tf_cnn_benchmark

############################################################################
# Script that runs the tf_cnn_benchmark via a udocker container.
# Passed arguments are parameters for the HPC SLURM job.
# Runs one benchmark run as well as one evaluation run and logs the output.
#
# NOTE: The udocker container needs to be set up with nvidia compatibility
############################################################################

##### DEFAULT CONFIG #####
UDOCKER_DIR="$PROJECT/.udocker" # Location of udocker and containers
CONTAINER="tf-gpu"	        # Container to run
# ------------------------
HOST_DATA="$PROJECT/datasets/cifar10"
HOST_LOG="$PROJECT/benchmarks_udocker"
HOST_OUTPUT="$PROJECT/benchmarks_udocker/output"
# ------------------------
UDOCKER_LOG="/benchmarks/log"
UDOCKER_DATA="/benchmarks/data"
UDOCKER_OUTPUT="/benchmarks/output"
# ------------------------


##### USAGEMESSAGE #####
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n
		--num_gpus  \t Amount of GPUs to run on \n
		--dataset   \t Dataset to use \n 
                --data_dir  \t Location of dataset \n
		--model     \t The CNN model to use \n
                --timestamp \t Timestamp for same batch of jobs \n
                --curr_iter \t Number of of current iteration"


##### PARSE SCRIPT FLAGS #####
arr=("$@")
if [ $# -eq 0 ]; then 
# use default config (0)
    break 
elif [ $1 == "-h" ] || [ $1 == "--help" ]; then 
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -eq 6 ]; then 
    # read benchmark options as parameters (6)
    for i in "${arr[@]}"; do
        [[ $i = *"--num_gpus"* ]]  && NUMGPUS=${i#*=} 
        [[ $i = *"--dataset"* ]]  && DATASET=${i#*=} 
        [[ $i = *"--data_dir"* ]] && HOST_DATA=${i#*=}
        [[ $i = *"--model"* ]]  && MODEL=${i#*=}
        [[ $i = *"--timestamp"* ]]  && TIMESTAMP=${i#*=}
        [[ $i = *"--curr_iter"* ]]  && CURR_ITER=${i#*=}
    done
else
    echo "ERROR! Wrong amount of arguments provided!"
    shopt -s xpg_echo    
    echo $USAGEMESSAGE
    exit 2
fi


##### Create local directories to mount #####
DIR_EXT="${DATASET}_${MODEL}_${NUMGPUS}_gpu/${CURR_ITER}" # Directory format for output files

HOST_LOG="${HOST_LOG}/bench_${TIMESTAMP}/${DIR_EXT}"
HOST_OUTPUT="${HOST_OUTPUT}/bench_${TIMESTAMP}/${DIR_EXT}"

mkdir -p $HOST_LOG
mkdir -p $HOST_OUTPUT


##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
OPTIONS="--batch_size=64 \
--num_batches=500 \
--optimizer=sgd \
--variable_update=parameter_server \
--local_parameter_device=cpu \
--train_dir=${UDOCKER_OUTPUT} \
--log_dir=${UDOCKER_LOG} \
--benchmark_log_dir=${UDOCKER_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--num_gpus=$NUMGPUS \
--model=$MODEL"

EVAL_OPTIONS=" --eval \
--train_dir=${UDOCKER_OUTPUT} \
--benchmark_log_dir=${UDOCKER_LOG} \
--num_gpus=$NUMGPUS \
--model=$MODEL"
#--eval_dir=$PWD/eval/ \ only for TFevent file

# If Data is not synthetic, use data at specified file location
if [ "$DATASET" != "synthetic" ]; then
    OPTIONS="${OPTIONS} --data_name=$DATASET --data_dir=$UDOCKER_DATA"
    EVAL_OPTIONS="${EVAL_OPTIONS} --data_name=$DATASET --data_dir=$UDOCKER_DATA"
fi


##### RUN THE JOB #####
echo "==================================="
echo "=> udocker container: $CONTAINER"
echo "=> Running on: $HOSTNAME"
echo "==================================="

echo "Setting up Nvidia compatibility."
nvidia-modprobe -u -c=0 # CURRENTLY only to circumvent a bug 
udocker setup --nvidia $CONTAINER   # Setup nvidia usage

echo "Running benchmark..."
SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $OPTIONS"  #benchmark script to run
udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_OUTPUT}:${UDOCKER_OUTPUT} -v ${HOST_LOG}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/benchmark.log #Rename since it is overwritten in eval run 

echo "Done with benchmark. Running evaluation..."
nvidia-modprobe -u -c=0 # CURRENTLY only to circumvent a bug 
SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $EVAL_OPTIONS"  #evaluation script to run
udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_OUTPUT}:${UDOCKER_OUTPUT} -v ${HOST_LOG}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/eval.log 


## Output files are not needed anymore
rm -r ${HOST_OUTPUT}

echo "Done. Logfiles written to: ${HOST_LOG}."
