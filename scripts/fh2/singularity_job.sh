#!/bin/sh
#SBATCH --time=3:00:00
#SBATCH --partition=visu
#SBATCH --nodes=1
#SBTACH --ntasks-per-node=48
#SBATCH --output=${PROJECT}/job.log
#SBATCH --job-name=tf_cnn_benchmark

############################################################################
# Script that runs the tf_cnn_benchmark via a singularity image.
# Passed arguments are parameters for the HPC SLURM job.
# Runs one benchmark run as well as one evaluation run and logs the output.
############################################################################

##### DEFAULT CONFIG #####
CONTAINER="tf_cnn_benchmark_gpu.simg"	# Image to run
# ------------------------
HOST_DATA="$PROJECT/datasets/cifar10"
HOST_LOG="$PROJECT/benchmarks_singularity"
HOST_OUTPUT="$PROJECT/benchmarks_singularity/output"
# ------------------------
SINGULARITY_LOG="/benchmarks/log"
SINGULARITY_DATA="/benchmarks/data"
SINGULARITY_OUTPUT="/benchmarks/output"
# ------------------------


##### USAGEMESSAGE #####
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n
		--num_gpus  \t Amount of GPUs to run on \n
		--dataset   \t Dataset to use \n 
                --data_dir  \t Location of dataset \n
		--model     \t The CNN model to use \n
                --timestamp \t Timestamp for same batch of jobs \n
                --curr_iter \t Number of of current iteration"


##### PARSE SCRIPT FLAGS #####
arr=("$@")
if [ $# -eq 0 ]; then 
# use default config (0)
    break 
elif [ $1 == "-h" ] || [ $1 == "--help" ]; then 
# print usagemessage
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -eq 6 ]; then 
# read benchmark options as parameters (1-3)
    for i in "${arr[@]}"; do
        [[ $i = *"--num_gpus"* ]]  && NUMGPUS=${i#*=} 
        [[ $i = *"--dataset"* ]]  && DATASET=${i#*=} 
        [[ $i = *"--data_dir"* ]] && HOST_DATA=${i#*=}
        [[ $i = *"--model"* ]]  && MODEL=${i#*=}
        [[ $i = *"--timestamp"* ]]  && TIMESTAMP=${i#*=}
        [[ $i = *"--curr_iter"* ]]  && CURR_ITER=${i#*=}
    done
else
    # Too many arguments were given (>4)
    echo "ERROR! Wrong amount of arguments provided!"
    shopt -s xpg_echo    
    echo $USAGEMESSAGE
    exit 2
fi

##### Create local directories to mount #####
DIR_EXT="${DATASET}_${MODEL}_${NUMGPUS}_gpu/${CURR_ITER}" # Directory format for output files

HOST_LOG="${HOST_LOG}/bench_${TIMESTAMP}/${DIR_EXT}"
HOST_OUTPUT="${HOST_OUTPUT}/bench_${TIMESTAMP}/${DIR_EXT}"

mkdir -p $HOST_LOG
mkdir -p $HOST_OUTPUT


##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
OPTIONS="--batch_size=64 \
--num_epochs=10 \
--optimizer=sgd \
--variable_update=parameter_server \
--train_dir=${SINGULARITY_OUTPUT} \
--log_dir=${SINGULARITY_LOG} \
--benchmark_log_dir=${SINGULARITY_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--num_gpus=$NUMGPUS \
--model=$MODEL"

EVAL_OPTIONS=" --eval \
--train_dir=${SINGULARITY_OUTPUT} \
--benchmark_log_dir=${SINGULARITY_LOG} \
--num_gpus=$NUMGPUS
--model=$MODEL"
#--eval_dir=$PWD/eval/ \ only for TFevent file

if [ "$DATASET" != "synthetic" ]; then
    # If Data is not synthetic, use data at specified file location
    OPTIONS="${OPTIONS} --data_name=$DATASET --data_dir=$SINGULARITY_DATA"
    EVAL_OPTIONS="${EVAL_OPTIONS} --data_name=$DATASET --data_dir=$SINGULARITY_DATA"
fi


##### RUN THE JOB #####
echo "======================================"
echo "=> Singularity container: $CONTAINER"
echo "=> Running on: $HOSTNAME"
echo "======================================"

nvidia-modprobe -u -c=0 # CURRENTLY only to circumvent a bug 

echo "Running benchmark..."
SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $OPTIONS"  #benchmark script to run
singularity exec --nv -B ${HOST_DATA}:${SINGULARITY_DATA},${HOST_OUTPUT}:${SINGULARITY_OUTPUT},${HOST_LOG}:${SINGULARITY_LOG} $CONTAINER $SCRIPT 
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/benchmark.log #Rename since it is overwritten in eval run 

echo "Done with benchmark. Running evaluation..."
SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $EVAL_OPTIONS"  #evaluation script to run
singularity exec --nv -B ${HOST_DATA}:${SINGULARITY_DATA},${HOST_OUTPUT}:${SINGULARITY_OUTPUT},${HOST_LOG}:${SINGULARITY_LOG} $CONTAINER $SCRIPT 
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/eval.log 


## Output files are not needed anymore
rm -r ${HOST_OUTPUT}

echo "Done. Logfiles written to: ${HOST_LOG}."
