#!/bin/bash

#TIMESTAMP=$(date +%s)
TIMESTAMP=$(date +%y%m%d_%H%M%S)
ThisHOST=$(hostname -i)

# ------------------------
UDOCKER_DATA="/benchmarks/data"
UDOCKER_RUN="/benchmarks/run_output"
# ------------------------

if [ "$TF_SERVER_TYPE" == "ps" ]; then
    echo $ThisHOST:$PARAM_SERVER_PORT >> $HOST_RUN/param_servers.list
    echo "[INFO] param_serverID=$TFServerID from $NumParamServers"
fi

if [ "$TF_SERVER_TYPE" == "worker" ]; then
    echo $ThisHOST:$WORKER_PORT >> $HOST_RUN/workers.list
    echo "[INFO] workerID=$TFServerID from $NumWorkers"
fi

# Check if all param server jobs has started
n_param_servers_on=0
while [ $n_param_servers_on -lt $NumParamServers ]
do
    sleep 5
    PARAM_SERVERS=($(cat $HOST_RUN/param_servers.list))
    n_param_servers_on=${#PARAM_SERVERS[@]}
done

# Check if all worker jobs has started
n_workers_on=0
while [ $n_workers_on -lt $NumWorkers ]
do
    sleep 5
    WORKERS=($(cat $HOST_RUN/workers.list))
    n_workers_on=${#WORKERS[@]}
done

# https://stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash
PARAM_SERVERS_LIST=$(IFS=, ; echo "${PARAM_SERVERS[*]}")
#echo $PARAM_SERVERS_LIST

WORKERS_LIST=$(IFS=, ; echo "${WORKERS[*]}")
#echo $WORKERS_LIST

# log directories
UDOCKER_OUTPUT=${UDOCKER_RUN}/${TF_SERVER_TYPE}${TFServerID}-output
UDOCKER_LOG=${UDOCKER_RUN}/${TF_SERVER_TYPE}${TFServerID}-log

# Define DATASET_OPTIONS depending on the dataset
if [ "$DATASET" != "synthetic" ]; then
    export DATASET_OPTIONS="--data_name=$DATASET --data_dir=$UDOCKER_DATA --num_epochs=$NUM_EPOCHS"
else
    export DATASET_OPTIONS="--num_batches=$NUM_BATCHES"
fi

OPTIONS="--batch_size=$BATCH_SIZE \
--optimizer=sgd \
--variable_update=distributed_replicated \
--cross_replica_sync=True \
--local_parameter_device=CPU \
--train_dir=${UDOCKER_OUTPUT} \
--log_dir=${UDOCKER_LOG} \
--benchmark_log_dir=${UDOCKER_LOG} \
--num_gpus=$NUM_GPUS \
--model=$MODEL \
$DATASET_OPTIONS \
--ps_hosts=$PARAM_SERVERS_LIST \
--worker_hosts=$WORKERS_LIST \
--benchmark_test_id=${TF_SERVER_TYPE}_${TIMESTAMP} \
--job_name=$TF_SERVER_TYPE \
--task_index=$TFServerID"


##### RUN THE JOB #####
echo "==================================="
echo "=> udocker container: $UCONTAINER"
echo "=> Running on: $ThisHOST"
echo "==================================="

echo "Setting up Nvidia compatibility."
nvidia-modprobe -u -c=0 # CURRENTLY only to circumvent a bug 
udocker setup --nvidia --force $UCONTAINER   # Setup nvidia usage

echo "Running benchmark..."
echo "python tf_cnn_benchmarks.py $OPTIONS"
SCRIPT="python /benchmarks/tf_cnn_benchmarks.py $OPTIONS"  #benchmark script to run
udocker run -v ${HOST_RUN}:${UDOCKER_RUN} ${UCONTAINER} $SCRIPT
