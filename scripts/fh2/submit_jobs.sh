####################################################################################
# Script to submit jobs for the tf_cnn_benchmark project on ForHLRII via sbatch.
# Uses containerized tensorflow benchmarks. Iterates over all possible parameter
# combinations for a certain number of iterations.
# Locations for datasets and logfiles have to be provided in the jobs themlseves.
####################################################################################

UDOCKER_JOB="udocker_job.sh"
SINGULARITY_JOB="singularity_job.sh"
USE_U=false
USE_S=false

TIMESTAMP="$(date +"%Y-%m-%d_%H-%M")"

##### USERINPUT #####
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n
		-u \t Submit udocker benchmark jobs \n
		-s \t Submit Singularity benchmark jobs \n
		-us \t Submit benchmark jobs via both \n
                -d \t Debug mode"

### Parse script flags ###
arr=("$@")
if [ $1 == "-h" ] || [ $1 == "--help" ]; then
    # print usagemessage
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -eq 1 ]; then
    for i in "${arr[@]}"; do
        [[ $i = *"-u"* ]]  && USE_U=true
        [[ $i = *"-s"* ]]  && USE_S=true
        [[ $i = *"-us"* ]]  && USE_U=true && USE_S=true 
        [[ $i = *"-d"* ]]  && break; # compute one debug run instead of anything else
    done
else
    # Too many arguments were given (>1 or 0)
    echo "ERROR! Too many arguments provided!"
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 2
fi


### DEBUG: Submit one single job to a fat node ###
if [ "$USE_U" = false ] && [ "$USE_S" = false ]
then
 # OUTPUT="$PROJECT/log/debug.log"
  mkdir -p ${PROJECT}/log/debug_${TIMESTAMP}
  OUTPUT="${PROJECT}/log/debug_${TIMESTAMP}/debug.log"
  HOST_DATA="$PROJECT/datasets/cifar10"
  params="--num_gpus=2 --dataset=cifar10 --model=resnet56 --timestamp=$TIMESTAMP --curr_iter=0 --data_dir=$HOST_DATA"
 
  echo "~~ Debug ~~"
 # sbatch --time=03:00:00 --output=$OUTPUT $UDOCKER_JOB $params
  sbatch --time=03:00:00 --output=$OUTPUT $SINGULARITY_JOB $params
  exit 1
fi


#### SUBMISSION ####
# Submit multiple jobs, one for each parameter permutation. 
# Take care of different datasets and container types.
# They usually take different run times
# Iterate the jobs NUM_ITERS times.
# Parameters for jobs to run ##

declare -a NUMGPUS=("1" "2" "4")
declare -a DATASET=("synthetic" "cifar10" "imagenet")
NUM_ITERS=1

if [ "$USE_U" = true ]
then
  mkdir -p ${PROJECT}/log/udocker_${TIMESTAMP}
fi
if [ "$USE_S" = true ]
then
  mkdir -p ${PROJECT}/log/singularity_${TIMESTAMP}
fi

i=0
counter=1
until [ $counter -gt $NUM_ITERS ]
do
  for num in "${NUMGPUS[@]}"
  do
    for data in "${DATASET[@]}"
    do
      # depending on data we need different cnn models
      if [ "$data" == "synthetic" ]
      then
        declare -a MODELNAME=("resnet50" "alexnet")
        HOST_DATA="$PROJECT/datasets/synthetic" #dummy directory
        TIMEREQ=2:00:00
  
      elif [ "$data" == "imagenet" ]
      then
        #declare -a MODELNAME=("resnet50" "alexnet")
        declare -a MODELNAME=("resnet50")
        HOST_DATA="$PROJECT/../eo9869/datasets/imagenet/data/"
        TIMEREQ=20:00:00
  
      elif [ "$data" == "cifar10" ]
      then
        declare -a MODELNAME=("resnet56" "alexnet")
        HOST_DATA="$PROJECT/datasets/cifar10"
        TIMEREQ=6:00:00
      fi
  
      for model in "${MODELNAME[@]}"
      do
        params="--num_gpus=$num --dataset=$data --model=$model --timestamp=$TIMESTAMP --curr_iter=$counter --data_dir=${HOST_DATA}"

        # UDOCKER JOB
        if [ "$USE_U" = true ]
        then
          echo "Submitting udocker job with parameters: $data, $model, $num gpus"
          OUTPUT="${PROJECT}/log/udocker_${TIMESTAMP}/${data}_${model}_${num}gpu_$counter.log"
          sbatch --time=$TIMEREQ --output=$OUTPUT $UDOCKER_JOB $params
          let i+=1
        fi
    
        # SINGULARITY JOB
        if [ "$USE_S" = true ]
        then
          echo "Submitting singularity job with parameters: $data, $model, $num gpus"
          OUTPUT="${PROJECT}/log/singularity_${TIMESTAMP}/${data}_${model}_${num}gpu_$counter.log"
          sbatch --time=$TIMEREQ --output=$OUTPUT $SINGULARITY_JOB $params
          let i+=1
        fi
      done
    done
  done
  let counter+=1
done

echo "Done. Submitted $i jobs."
